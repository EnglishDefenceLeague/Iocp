#include "OverlappedExtended.hpp"
OverlappedExtended::OverlappedExtended
(
	Iocp::OverlappedOperation Operation,
	AbstractSocket* InternalSocket,
	Io* Trasnfer,
	bool CompleteOnAll,
	PVOID ExtraExternal,
	PVOID ExtraInternal,
	PVOID CompletionRoutine
)
{
	this->InternalSocket = InternalSocket;
	this->CompleteOnAll = CompleteOnAll;
	this->ExtraExternal = ExtraExternal;
	this->ExtraInternal = ExtraInternal;
	this->Operation = Operation;
	this->CompletionRoutine = CompletionRoutine;
	this->Transfer = Transfer;
}
LPOVERLAPPED OverlappedExtended::GetOverlapped()
{
	return (LPOVERLAPPED)this;
}
void OverlappedExtended::CleanOverlapped()
{
	RtlSecureZeroMemory(this, sizeof(OVERLAPPED));
}
