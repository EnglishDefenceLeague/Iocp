#ifndef _EXTERNALSOCKET_HPP_
#define _EXTERNALSOCKET_HPP_
#include "Base.hpp"
#include <queue>
using std::queue;

class ExternalSocket
	:
	public AbstractSocket
{
private:
	
	SOCKET Connection;
	//queue<Io*> OutgoingQueue;
	//bool IsWriting;
public:
	ExternalSocket();
	SOCKET GetSocket();
	void Reserved(PVOID Reserved)//Only to be used for inner classes
	{
		this->Connection = (SOCKET)Reserved;
	}
	~ExternalSocket();
	bool Read
	(
		Io* Packet,
		bool CompleteOnAll,
		PVOID ExtraExternal,
		Iocp::ReadWriteCompletionRoutine Completion
	);
	
	bool Write
	(
		Io* Packet,
		bool CompleteOnAll,
		PVOID ExtraExternal,
		Iocp::ReadWriteCompletionRoutine Completion
	);

	bool ReadWriteInternal(AbstractOverlapped * Overlapped);

private:
protected:

};
#endif