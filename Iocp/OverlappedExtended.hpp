#ifndef _OVERLAPPED_EXTENDED_H_
#define _OVERLAPPED_EXTENDED_H_

#include "Packet.hpp"
namespace Iocp
{
	typedef VOID(__cdecl* ReadWriteCompletionRoutine)
		(
			AbstractSocket* Abstract,
			uint32_t Error,
			Io* Packet,
			PVOID ExtraForCallback
			);




	typedef VOID(__cdecl* AcceptExCompletionRoutine)
		(
			AbstractSocket* Abstract,
			LPSOCKADDR ClientAddressLocal,
			int32_t ClientAddressLocalSize,
			LPSOCKADDR ClientAddressRemote,
			int32_t ClientAddressRemoteSize,
			PVOID ExtraForCallback
			);

	enum OverlappedOperation
	{
		Accept = 1,
		Read,
		Write
	};
};
class OverlappedExtended
	:
	public AbstractOverlapped
{
public:
	AbstractSocket* InternalSocket;
	bool CompleteOnAll;
	PVOID ExtraExternal;
	PVOID ExtraInternal;
	Iocp::OverlappedOperation Operation;
	PVOID CompletionRoutine;
	Io* Transfer;

	OverlappedExtended
	(
		Iocp::OverlappedOperation Operation,
		AbstractSocket* InternalSocket,
		Io* Transfer,
		bool CompleteOnAll,
		PVOID ExtraExternal,
		PVOID ExtraInternal,
		PVOID CompletionRoutine
	);


	LPOVERLAPPED GetOverlapped();
	void CleanOverlapped();
private:

};
#endif