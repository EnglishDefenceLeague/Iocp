#ifndef _SERVEREXAMPLE_HPP_
#define _SERVEREXAMPLE_HPP_
#include "Acceptor.hpp"
#include <vector>

class Server
{
private:
	Base& Internal;
	Acceptor Accept;
	std::vector<ExternalSocket*> Connections;
public:
	Server(Base& Iocp)
		:
		Internal(Iocp),
		Accept(Iocp,"8080")
	{
		ExternalSocket* Connection = new ExternalSocket();
		if (!Accept.Accept(Connection, this, AcceptCompletionRoutine))
			printf("Failed to accept!\n");
	}
	static VOID AcceptCompletionRoutine
	(
		AbstractSocket* Abstract,
		LPSOCKADDR ClientAddressLocal,
		int32_t ClientAddressLocalSize,
		LPSOCKADDR ClientAddressRemote,
		int32_t ClientAddressRemoteSize,
		PVOID ExtraForCallback
	)
	{
		Server* This = (Server*)ExtraForCallback;
		ExternalSocket* Connection = dynamic_cast<ExternalSocket*>(Abstract);
		This->GetConnections().push_back(Connection);
		printf("Something connected!:%s\n", ((LPSOCKADDR_IN)ClientAddressRemote)->sin_addr);

		ExternalSocket* Incoming = new ExternalSocket();
		This->Accept.Accept(Incoming, This,AcceptCompletionRoutine);
	}
	~Server()
	{

	}
private:
	std::vector<ExternalSocket*>& GetConnections()
	{
		return Connections;
	}
};

#endif