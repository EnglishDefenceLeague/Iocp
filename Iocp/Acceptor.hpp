#ifndef _ACCEPTOR_HPP_
#define _ACCEPTOR_HPP_
#include "ExternalSocket.hpp"
#include <string>

class Acceptor
	:
	public AbstractSocket
{
private:
	Base& InternalBase;
	SOCKET Connection;
	HANDLE CompletionPort;
	ADDRINFO* Result;
	const std::string& PortInternal;
	LPFN_ACCEPTEX AcceptExInternal;
	LPFN_GETACCEPTEXSOCKADDRS GetAcceptExSockAddrsInternal;
public:
	Acceptor
	(
		Base& InternalBase,
		const std::string& Port
	);
	~Acceptor();
	SOCKET GetSocket();
	/*
	typedef VOID(__cdecl* AcceptExCompletionRoutine)
		(
			AbstractSocket* Abstract,
			LPSOCKADDR ClientAddressLocal,
			int32_t ClientAddressLocalSize,
			LPSOCKADDR ClientAddressRemote,
			int32_t ClientAddressRemoteSize,
			PVOID ExtraForCallback
			);
			*/
	bool Accept
	(
		ExternalSocket* Socket,
		PVOID Extra,
		Iocp::AcceptExCompletionRoutine Completion
	);
	
private:
	void CreateAddressInformationWithPassive(ADDRINFO* Information);
	bool CreateSocket
	(
		const std::string& Port,
		ADDRINFO* Information
	);
	bool Listen();
	bool ReadWriteInternal(AbstractOverlapped* Overlapped)
	{

	}
	bool CreateServerSocket();
protected:
	


	
};
#endif