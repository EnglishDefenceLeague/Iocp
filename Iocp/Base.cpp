#include "Base.hpp"
Base::Base()
{
	StartWsa();
	CompletionPort = AssociateCompletionPort(INVALID_HANDLE_VALUE, NULL, 0);
	CreateCompletionThreads();
}
Abstract* Base::This()
{
	return this;
}
Base::~Base()
{
	WSACleanup();
	//Close all threads once every event is satisfied
}
bool Base::CreateCompletionThreads()
{
	uint32_t Index = 0;
	for (; Index < sizeof(CompletionThreads) / sizeof(ThreadBlock); ++Index)
	{
		CompletionThreads[Index].ThreadHandle = CreateThread
		(
			NULL, 
			0, 
			(LPTHREAD_START_ROUTINE)CompletionThread, 
			this, 
			0, 
			(LPDWORD)&CompletionThreads[Index].ThreadId
		);
		if (!CompletionThreads[Index].ThreadHandle)
			return 0;
		
	}
	return 1;
}
HANDLE Base::GetCompletionPort()
{
	return CompletionPort;
}

HANDLE Base::AssociateCompletionPort
(
	HANDLE Port, 
	HANDLE ExistingPort, 
	PVOID Key
)
{
	HANDLE CompletionPort = CreateIoCompletionPort
	(
		Port, 
		ExistingPort,
		(ULONG_PTR)Key,
		0
	);

	return CompletionPort;
}

HANDLE Base::AssociateCompletionPort
(
	HANDLE Port, 
	PVOID Key
)
{
	return AssociateCompletionPort
	(
		Port, 
		CompletionPort, 
		Key
	);
}

bool Base::StartWsa()
{
	if (!WSAStartup(0x202, &Wsa))
		return true;

	return false;
}
void Base::AbstractHandleAcceptInternal(OverlappedExtended * Overlapped)
{

	SOCKADDR AddressLocal = { 0 };
	int32_t AddressLocalSize = sizeof(SOCKADDR);

	SOCKADDR AddressRemote = { 0 };
	int32_t AddressRemoteSize = sizeof(SOCKADDR);

	Iocp::AcceptExCompletionRoutine CompletionRoutine = (Iocp::AcceptExCompletionRoutine)Overlapped->CompletionRoutine;

	AssociateCompletionPort
	(
		(HANDLE)Overlapped->InternalSocket->GetSocket(), 
		Overlapped->InternalSocket
	);

	getsockname
	(
		Overlapped->InternalSocket->GetSocket(), 
		&AddressLocal, 
		&AddressLocalSize
	);

	getpeername
	(
		Overlapped->InternalSocket->GetSocket(),
		&AddressRemote, 
		&AddressRemoteSize
	);


	CompletionRoutine
	(
		Overlapped->InternalSocket, 
		&AddressLocal, 
		AddressLocalSize, 
		&AddressRemote, 
		AddressRemoteSize, 
		Overlapped->ExtraExternal
	);

	delete Overlapped;
}
	

VOID Base::WsaCompletionRoutine
(
	ULONG32 Error, 
	ULONG32 Transferred, 
	OverlappedExtended*Extended
)
{
	Iocp::ReadWriteCompletionRoutine CompletionRoutine = (Iocp::ReadWriteCompletionRoutine)Extended->CompletionRoutine;


	Extended->Transfer->UpdateOffset(Transferred);

	if (Error || !Extended->CompleteOnAll || Extended->Transfer->IsComplete())
	{

		CompletionRoutine
		(
			Extended->InternalSocket, 
			Error, 
			Extended->Transfer, 
			Extended->ExtraExternal
		);
		delete Extended;
		return;
	}

	Extended->CleanOverlapped();

	if (Extended->InternalSocket->ReadWriteInternal(Extended))
		return;
	
	CompletionRoutine
	(
		Extended->InternalSocket,
		Error,
		Extended->Transfer,
		Extended->ExtraExternal
	);


	delete Extended;

}
#include <stdio.h>

uint32_t CALLBACK Base::CompletionThread(PVOID Context)
{
	Base* Self = (Base*)Context;
	while (1)
	{
		OverlappedExtended* Overlapped = NULL;
		PVOID Key = 0;
		uint32_t SizeOfCompletion = 0;
		int32_t Status = 0;

		printf("Starting %x\n", GetCurrentThreadId());
		Status = GetQueuedCompletionStatus
		(
			Self->GetCompletionPort(), 
			(PULONG)&SizeOfCompletion,
			(PULONG_PTR)&Key, 
			(LPOVERLAPPED*)&Overlapped, 
			INFINITE
		);
		printf("%x returned\n", GetCurrentThreadId());
		if (!Overlapped)
			return 0;

		if (!Key)
			return 0;

		if (!Status)
			return 0;

		if (Overlapped->Operation == Iocp::OverlappedOperation::Accept)
		{
			Self->AbstractHandleAcceptInternal(Overlapped);
		}
		else
		{
			Self->WsaCompletionRoutine
			(
				Status != 0 ? 0 : Status,
				SizeOfCompletion,
				Overlapped
			);
		}
			


	}
	return 1;

}

VOID Base::InitializeAcceptEx
(
	SOCKET Socket,
	LPFN_ACCEPTEX* AcceptExPointer
)
{
	GUID GuidAcceptEx = WSAID_ACCEPTEX;
	uint32_t Returned = 0;


	WSAIoctl
	(
		Socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GuidAcceptEx,
		sizeof(GUID),
		&AcceptExPointer,
		sizeof(GuidAcceptEx),
		(PULONG)&Returned,
		0,
		0
	);

}
VOID Base::InitializeGetacceptExSockAddrs
(
	SOCKET Socket,
	LPFN_GETACCEPTEXSOCKADDRS GetAcceptExSockAddrsPointer
)
{
	GUID GUIDGetAcceptExSockaddrs = WSAID_GETACCEPTEXSOCKADDRS;
	uint32_t Returned = 0;

	WSAIoctl
	(
		Socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GUIDGetAcceptExSockaddrs,
		sizeof(GUID),
		&GetAcceptExSockAddrsPointer,
		sizeof(GUIDGetAcceptExSockaddrs),
		(PULONG)&Returned,
		0,
		0
	);

}
LPFN_CONNECTEX Base::InitializeConnectEx(SOCKET Socket)
{
	GUID GUIDConnectEx = WSAID_CONNECTEX;
	uint32_t Returned = 0;
	LPFN_CONNECTEX ConnectExPointer;

	WSAIoctl
	(
		Socket,
		SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GUIDConnectEx,
		sizeof(GUID),
		&ConnectExPointer,
		sizeof(GUIDConnectEx),
		(PULONG)&Returned,
		0,
		0
	);

	return ConnectExPointer;
}
