#ifndef _ABSTRACTOVERLAPPED_HPP_
#define _ABSTRACTOVERLAPPED_HPP_
#include "Abstract.hpp"
class AbstractOverlapped
	:
	public OVERLAPPED
{
public:
	virtual LPOVERLAPPED GetOverlapped() = 0;
};

#endif