#include "Acceptor.hpp"

Acceptor::Acceptor
(
	Base& Iocp,
	const std::string& Port
)
	:
	InternalBase(Iocp),
	PortInternal(Port)
{
	Connection = INVALID_SOCKET;
	CompletionPort = NULL;
	Result = NULL;
	InternalBase.InitializeAcceptEx(GetSocket(),&AcceptExInternal);
	InternalBase.InitializeGetacceptExSockAddrs(GetSocket(),GetAcceptExSockAddrsInternal);
	
}

Acceptor::~Acceptor()
{
	if (Connection != INVALID_SOCKET)
	{
		closesocket(Connection);
		Connection = INVALID_SOCKET;
	}
	if (Result)
	{
		freeaddrinfo(Result);
		Result = NULL;
	}
}

SOCKET Acceptor::GetSocket()
{
	return Connection;
}

bool Acceptor::Accept
(
	ExternalSocket* Socket,
	PVOID Extra,
	Iocp::AcceptExCompletionRoutine Completion
)
{

	
	SOCKET Incoming = socket
	(
		AF_INET,
		SOCK_STREAM,
		IPPROTO_TCP
	);
	if (Incoming == INVALID_SOCKET)
		return false;
	ULONG Redundant = 0;
	Socket->Reserved((PVOID)Incoming);
	OverlappedExtended* Overlapped = new OverlappedExtended
	(
		Iocp::OverlappedOperation::Accept,
		Socket,
		NULL,
		false,
		Extra,
		this,
		Completion
	);


	if (!AcceptExInternal
	(
		GetSocket(),
		Incoming,
		NULL,
		0,
		0,
		0,
		&Redundant,
		Overlapped
	))
		return false;


	return true;

}


bool Acceptor::CreateServerSocket()
{
	ADDRINFO Information = { 0 };
	CreateAddressInformationWithPassive(&Information);

	if (!CreateSocket(PortInternal, &Information))
		return false;

	if (!Listen())
		return false;

	InternalBase.AssociateCompletionPort
	(
		(HANDLE)Connection, 
		this
	);


	return true;
}


void Acceptor::CreateAddressInformationWithPassive(ADDRINFO* Information)
{
	Information->ai_protocol = IPPROTO_TCP;
	Information->ai_socktype = SOCK_STREAM;
	Information->ai_family = AF_INET;
	Information->ai_flags = AI_PASSIVE;
}
bool Acceptor::Listen()
{
	if (bind(Connection, Result->ai_addr, Result->ai_addrlen))
		return false;
	if (listen(Connection, SOMAXCONN))
		return false;


	return true;
}
bool Acceptor::CreateSocket
(
	const std::string& Port,
	ADDRINFO* Information
)
{
	if (getaddrinfo(NULL, Port.c_str(), Information, &Result))
		return false;

	Connection = socket
	(
		Result->ai_family, 
		Result->ai_socktype, 
		Result->ai_protocol
	);

	if (Connection == INVALID_SOCKET)
		return false;


	return true;
}
