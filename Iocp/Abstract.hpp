#ifndef _ABSTRACT_HPP_
#define _ABSTRACT_HPP_
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <WS2tcpip.h>
#include <stdint.h>
#pragma comment(lib,"WS2_32")

class Abstract
{
public:
	virtual Abstract* This() = 0;
};

#endif